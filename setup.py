#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A setup module for defuguer"""

from setuptools import setup, find_packages

from codecs import open
import os

here = os.path.abspath(os.path.join(os.getcwd(),'setup.py'))

