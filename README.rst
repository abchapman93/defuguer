#!/usr/bin/env python3
# -*- coding: utf-8 -*-

This is a README file for defuguer.

This project aims to provide the user tools to analyze Bach's fugue in C# major
using graphical analysis and calculations of chromaticism. Included in the 
package is a notebook, defuguer_demo, that has the most important modules
imported and explanations of the various functions.
Following is a general description of the modules, directories, and goals of 
the project:

**df_objects**
My classes that I created were Note and Measure objects.

Notes have the following attributes:
    pitch: the letter name of the note (A-G#)
    value: the integer associated with that pitch (0-11)
    octave: which octave the note is in on a keyboard (0-6)
    absolute: the pitch on the piano as determined by its octave and value (0-87)
Measures have the following attributes:
    numeric: the measure with notes represented by rests and integers (0-11)
    absolute: the measure with notes represented by rests and integers (0-87)
    measure_num: the index of the measure in the piece, starting at 1
    tonality: the primary key of the measure. Once I stopped focusing on the 
    theme_finder task, the tonality became less relevant. 
Motives were a class I created and would have used with theme_matcher.
They inherit from the measure class. Because I did not do anything more with 
theme matching, I never used this class.
    
**formatting**
This module deals with the representation of notes and measures. This involves:
1) Taking the basic information about a note (pitch name, rhythm, and octave)
and creating a Note Object out of this. These are then organized into measures,
which are objects consisting of lists of notes. 
2) Writing these data into .tsv files that are saved in the directory "score". 
These files contain data for all measures in the piece, organized by voice 
(soprano, tenor, and bass.) I used this file format instead of populating a database.
3) Retrieving information from the tsv files in order to perform the functions 
and methods as described below.

**score**
Contains the .tsv files for each voice. I was initially planning on creating a 
mysqlite database, but we were told in class that any sort of data storage format
would suffice. 

**chromaticism**
This module provides methods to measure the chromaticism of a given measure.
"Chromaticism" is defined as how many notes in the measure are not in C# major,
the primary key of the piece. (C#, D#, E#, F#, G#, A#, B#). 

**graph_measures**
This module provides graphical depictions of the musical material of the piece 
and of the chromaticism. This gives the user a visual understanding of how the 
piece develops and is structured. The functions allow to see a specific range of
measures and to view all 3 voices or fewer.

**musikgeist**
This is my user interface that can be used outside of the Jupyter Notebook. It
provides a few, relatively limited uses of the functions defined in other modules.

**theme_matching**
This was initially intended to find instances of musical motives throughout the
piece, but it ended up being above the scope of this project. It could really
be an entire project on its own.