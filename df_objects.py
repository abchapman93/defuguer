#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:17:03 2016

@author: alec
"""

class Note(object):
    def __init__(self, pitch=None,length=1,value=0,octave=4):
        self.__pitch= pitch
        self.__length= length
        self.__value= 0
        self.__octave=octave
        self.set_value(pitch)
    def set_value(self,pitch):
        twelve_tones = {
    "C":0,
    "Db": 1, 
    "C#": 1,
    "D":2,
    "D#":3,
    "Eb":3,
    "E":4,
    "E#":5,
    "F":5,
    "F#":6,
    "Gb":6,
    "G":7,
    "G#":8,
    "Ab":8,
    "A":9,
    "A#":10,
    "Bb":10,
    "B":11,
    "B#":0,
    "r":"R",
    "R":'R'}
        self.__value = twelve_tones[pitch]
    def get_pitch(self):
        return self.__pitch
    def get_length(self):
        return self.__length
    def get_value(self):
        return self.__value
    def get_octave(self):
        return self.__octave
    def realize(self):
        """returns a a numerics with the note represented in sixteenth notes"""
        realized_measure = []
        for x in range(0,self.__length):
            try:
                realized_measure.append(int(self.__value))
            except ValueError:
                realized_measure.append(self.__value)
        return realized_measure

    def transpose2C(self,tonality):
        """transposes from current key, the integer "tonality", to the key of C"""
        return self.__value-tonality
        
    def abs_pitch(self):
        """returns the index of the note on a piano keyboard (0-87)"""
        try:
            abs_note = ((self.__octave*12-12)+self.__value)+3
        except TypeError:
            abs_note = self.__pitch
        return abs_note
        
class measure(object): 
    def __init__(self,numeric=None,tonality=0,measure_num=0,absolute=None):

        measure.tonality=int(tonality)
        measure.measure_num=int(measure_num)
        measure.absolute=absolute
        measure.numeric=numeric
        self.set_numeric()
        self.set_absolute()
    def get_retro(self):
        return self.numeric[::-1]
    def get_tonality(self):
        return self.tonality
    def get_measure_num(self):
        return self.measure_num
    
    
    def set_numeric(self):
        numeric_measure=[]
        for x in self.numeric.split(','):
            try:
                numeric_measure.append(int(x))
            except ValueError:
                numeric_measure.append(x)
        self.numeric = numeric_measure
    def set_absolute(self):
        try:
            absolute_measure=[]
            for x in self.absolute.split(','):
                try:
                    absolute_measure.append(int(x))
                except ValueError:
                    absolute_measure.append(x)
            self.absolute = absolute_measure
        except AttributeError:
            return
    def get_numeric(self):
        return self.numeric
    def get_absolute(self):
        return self.absolute
        
    def transpose2C(self):
        """Takes a numeric measureand its tonality.
        Returns a list transposed to C"""
        interval = 0 - self.tonality
        new_measure = []
        for note in self.numeric:
            try:
                new_note = int(note) + interval
                if new_note < 0:
                    new_note = new_note + 12
                if new_note >= 12:
                    new_note = new_note - 12
                new_measure.append(new_note)
            except ValueError:
                new_measure.append(note)
            except TypeError:
                new_measure.append(note)
        return new_measure
    
        
    def neutralize(self):
        """Transposes to C based on the first note of a measure"""
        first_pitch = [x for x in self.numeric if isinstance(x,int)][0]
        interval = 0 - first_pitch
        new_measure = []
        for note in self.numeric:
            try:
                new_note = int(note) + interval
                if new_note < 0:
                    new_note = new_note + 12
                if new_note >= 12:
                    new_note = new_note - 12
                new_measure.append(new_note)
            except ValueError:
                new_measure.append(note)
        return new_measure
        
    def transpose(self, d=None, k=None):
        """Transposes motive into a different key. 
        Can use either distance (d), which will move n-half steps,
        or key (k), which will transpose to that key.
        k will only work correctly if the motive begins on the tonic"""
        if d != None and k != None:
            raise ValueError("You cannot use both distance and key.")
        else:
            if d != None:
                new_measure = []
                for note in self.numeric:
                    if isinstance(note,int):
                        new_note = note + d
                        if new_note >= 12:
                            new_note = new_note-12
                            new_measure.append(new_note)
                        if new_note < 0:
                            new_note = new_note+12
                            new_measure.append(new_note)
                        else:
                            new_measure.append(new_note)
                    else:
                        new_note = note
                        new_measure.append(new_note)
                return new_measure
            if k != None:
                note_range = k - self[0].numeric #find distance between first note and the new key
                new_measure = []
                for note in self.numeric:
                    try:
                        new_note = note+note_range
                        if new_note > 12:
                            new_note = new_note-12
                            new_measure.append(new_note)
                        if new_note < 0:
                            new_note = new_note+12
                            new_measure.append(new_note)
                        else:
                            new_measure.append(new_note)
                    except TypeError:
                        new_note = note
                        new_measure.append(new_note)
                return new_measure
            if TypeError:
                pass
                    
            
        
    def invert(self): 
        """Provides a mirror image of the motive. Default inversion point is the first note.
        If the first note is a rest, inversion_point should be manually provided"""
        inverted_motive = []
        int_list = [x for x in self.numeric if isinstance(x,int)]
        n = int_list[0]
        for note in self.numeric:
            try:
            #if isinstance(note,int):
                inverted_note = (n - note) + n
                
                if inverted_note < 0:
                    inverted_note = inverted_note + 12 
                if inverted_note > 11:
                    inverted_note = inverted_note - 12
            except ValueError:
                inverted_note = note
            except TypeError:
                inverted_note = note
            inverted_motive.append(inverted_note)
            #print(note,n,inverted_motive)
        return inverted_motive
    
    #clean up these magic methods

    def __str__(self):
        print(self.numeric)
        
    def __add__(self,other):
        new_measure = []
        for x in self:
            if isinstance(x,int):
                try:
                    new_measure.append(x + other[self.index(x)])
                except TypeError:
                    new_measure.append(x)
            else:
                try:
                    new_measure.append(other[self.index(x)])
                except TypeError:
                    new_measure.append(x)
        return new_measure
        
class motive(measure):
    def __init__(self,numeric=None,tonality=0,measure_num=0,absolute=None,element=None):
        super(measure,self).__init__(numeric=numeric,tonality=tonality,
            measure_num=measure_num,absolute=absolute)
        self.element=element
        def get_element(self):
            return self.element
    def __lt__(self,p):
        if self.element == "Subject":
            self_value = 4
        if self.element == "Answer":
            self_value = 3
        if self.element == "Countersubject":
            self_value = 2
        if self.element == "Episode":
            self_value = 1
        if p.element == "Subject":
            p_value = 4
        if p.element == "Answer":
            p_value = 3
        if p.element == "Countersubject":
            p_value = 2
        if p.element == "Episode":
            p_value = 1
        return self_value < p_value
    def __gt__(self,p):
        if self.element == "Subject":
            self_value = 4
        if self.element == "Answer":
            self_value = 3
        if self.element == "Countersubject":
            self_value = 2
        if self.element == "Episode":
            self_value = 1
        if p.element == "Subject":
            p_value = 4
        if p.element == "Answer":
            p_value = 3
        if p.element == "Countersubject":
            p_value = 2
        if p.element == "Episode":
            p_value = 1
        return self_value > p_value
    def __etq__(self,p):
        if self.element == "Subject":
            self_value = 4
        if self.element == "Answer":
            self_value = 3
        if self.element == "Countersubject":
            self_value = 2
        if self.element == "Episode":
            self_value = 1
        if p.element == "Subject":
            p_value = 4
        if p.element == "Answer":
            p_value = 3
        if p.element == "Countersubject":
            p_value = 2
        if p.element == "Episode":
            p_value = 1
        return self_value == p_value