#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 15:02:36 2016

@author: alec
"""
from numpy import arange
import matplotlib.pyplot as plt
import formatting as fm
tsv2Measure = fm.tsv2Measure
#use conv_measure
def graph_measure(measure_in,count=1):
    """takes a single measure in list form and returns an x and y axis as lists"""
    one_measure = arange(0,1,1/16)
    
    x_axis = []
    
    #absolute_measure = measure.get_numeric()
    for x in range(0,len(measure_in)):
        if isinstance(measure_in[x],int):
            x_axis.append(one_measure[x])
    y_axis = [x for x in measure_in if isinstance(x,int)]
    #plt.plot(x_axis,y_axis)
    return (x_axis,y_axis)
    
def graph_measures(measures_num,soprano=True,tenor=True,bass=True):
    """Takes a range of measures as a tuple and returns a graph of those measures.
    By default graphs all three voices."""
    #set up space for widget and graph
    fig, ax = plt.subplots()
    plt.subplots_adjust(left=0.25, bottom=0.25)
    
    voices = []
    output = []
    if soprano == True:
        soprano_x = []
        soprano_y = []
        voices.append('soprano = blue')
        measures_for_graph = []
        for number in [x for x in range(measures_num[0],measures_num[1]+1)]:
            measure_object=tsv2Measure('soprano',number)
            absolute_measure=measure_object.get_absolute()
            measures_for_graph.append(absolute_measure)

            for item in graph_measure(absolute_measure)[0]:
                try:
                    soprano_x.append(item+number)
                except IndexError:
                    print('failed: ', number, item) 
            for item in graph_measure(absolute_measure)[1]:
                    soprano_y.append(item)
        plt.plot(soprano_x,soprano_y,'b',label='soprano')
    
        
    if tenor == True:
        tenor_x = []
        tenor_y = []
        voices.append('tenor = red')
        measures_for_graph = []
        for number in [x for x in range(measures_num[0],measures_num[1]+1)]:
            measure_object=tsv2Measure('tenor',number)
            absolute_measure=measure_object.get_absolute()
            measures_for_graph.append(absolute_measure)

            for item in graph_measure(absolute_measure)[0]:
                try:
                    tenor_x.append(item+number)
                except IndexError:
                    print('failed: ', number, item) 
            for item in graph_measure(absolute_measure)[1]:
                    tenor_y.append(item)
    
        plt.plot(tenor_x,tenor_y,'r',label='tenor')
    if bass == True:
        bass_x = []
        bass_y = []
        voices.append('bass = green')
        measures_for_graph = []
        for number in [x for x in range(measures_num[0],measures_num[1]+1)]:
            measure_object=tsv2Measure('bass',number)
            absolute_measure=measure_object.get_absolute()
            measures_for_graph.append(absolute_measure)

            for item in graph_measure(absolute_measure)[0]:
                try:
                    bass_x.append(item+number)
                except IndexError:
                    print('failed: ', number, item) 
            for item in graph_measure(absolute_measure)[1]:
                    bass_y.append(item)
        plt.plot(bass_x,bass_y,'g',label='bass')
        
    
    #plt.legend(loc='upper')
    plt.xlabel('measure')
    plt.ylabel('pitch')
    plt.show()
    print(voices)
    return
