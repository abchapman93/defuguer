#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:13:05 2016

@author: alec
"""

import os 

import df_objects as do
Note = do.Note
measure = do.measure
#soprano_old = [8,8,8,8,8,8,8,8,10,8,6,8,5,5,1,1]
#soprano1 = 'r 6, G# 2, A#, G#, F#, G#, E# 2 5, C# 2 5'
def note_generator(string,measure_octave=4):
    """Takes a string and returns a list of Note objects.
	Notes in the strings should be in groups which are separated by commas:
	“*Note name* *length* *octave*, etc…
	length and octave are 1 and measure_octave by default."""
    csv = string.split(',')
    output = []
    for value in csv:
        #print(x, len(x))
        note_data = []
        x = value.split()
        if len(x) == 1:
            note_data.append(x[0])
            note_data.append(1)
            note_data.append(measure_octave)
        if len(x) == 2:
            note_data.append(x[0])
            note_data.append(int(x[1]))
            note_data.append(measure_octave)
        if len(x) == 3:
            note_data.append(x[0])
            note_data.append(int(x[1]))
            note_data.append(int(x[2]))
        #print((note_data[0],type(note_data[0])), (note_data[1],type(note_data[1]),
               #(note_data[2],type(note_data[2]))))
        #Note = do.Note()
        generated_note = Note(pitch=note_data[0],length=int(note_data[1]),octave=int(note_data[2]))
        output.append(generated_note)
    return output
        #generated_note = Note(length=length_in,octave=octave_in)
        #output.append(generated_note)
    #return output
    

def measure_outputter(measure_num,measure,tonality=1):
    """Takes a list of notes and returns as output:
        A numeric form of the measure
        A string form of the measure #NOTE: without rhythm
        An actual form of the measure
        The tonality."""
    string_measure = ''
    for note in measure:
        try:
            string_measure+=' ' + note.get_pitch()
        except AttributeError:
            return note
        #print(note.realize())
    list_of_lists = [note.realize() for note in measure]
    #print(list_of_lists)
    numeric_measure = []
    for item in list_of_lists:
        for x in item:
            numeric_measure.append(x)
    abs_measure = []
    for note in measure:
        for x in range(0,note.get_length()):
            abs_measure.append(note.abs_pitch())
    #abs1 = [note.abs_pitch() for note in formatted]
    return measure_num, numeric_measure, abs_measure, tonality

#create a function to write into tsv
def measures2tsv(voice,measures, DATADIR=os.path.join(os.path.expanduser('~'),'abc_6950_HW','defuguer','score')):
    """Writes all elements from measure_outputter and writes it into a tsv file"""
    #create one long string
    output = ''
    measure_num = str(measures[0])
    output += measure_num + '\t'
    for measure in measures[1:3]:
        #string_measure = [str(x) for x in measure]
        for x in measure[:-1]:
            output += str(x) + ','
        output += str(measure[-1])+'\t'
    tonality = str(measures[-1])
    output += tonality + '\n'
    
    #write string to file
    #file names: soprano.tsv, tenor.tsv, bass.tsv
    fname = os.path.join(DATADIR, voice+'.tsv')
    f0 = open(fname,'a')
    f0.write(output)
    f0.close()
    return fname
    
    
twelve_tones = {
    "C":0,
    "Db": 1, #what about enharmonic equivalents? 
    "C#": 1,
    "D":2,
    "D#":3,
    "Eb":3,
    "E":4,
    "E#":5,
    "F":5,
    "F#":6,
    "Gb":6,
    "G":7,
    "G#":8,
    "Ab":8,
    "A":9,
    "A#":10,
    "Bb":10,
    "B":11,
    "B#":0,
    "r": 'R',
    'R':'R'
    }

def score_wizard():
    """Assists in the manual entry of the score.
    Writes input into a .tsv file"""
    #Welcome page
    action = input('''Hello there! I'm the score wizard and will help you enter
    in the musical score as painlessly as possible. How can I help you?
    1 - Enter a new measure
    2 - Help
    Q - Quit\n''')
    
    #prepare data to be passed into notes
    if action == str(1):
        vs = input('which voice will you be writing for?\n Soprano \t Tenor \t Bass\n')
        if vs.lower() == 's' or vs.lower() == 'soprano':
            measure_voice = 'soprano'
        elif vs.lower() == 't' or vs.lower() == 'tenor' or vs.lower() == 'm' or vs.lower() ==  'middle':
            measure_voice = 'tenor'
        elif vs.lower() == 'b' or vs.lower() == 'bass':
            measure_voice = 'bass'
        else:
            vs = input('Please enter a proper voice name') #will this loop back?
        measure_number = int(input('What measure will you be starting on?\n'))
        def note_entry(m_voice=measure_voice,m_num=measure_number):
            entry = input('''Please enter the following information separated by slashes:
                Tonality:
                Primary Octave:
                The Measure -- pitch, length [default=1], octave [default=Primary Octave]\n
                To quit, type 'Q'\n''')
            if entry.lower() != 'q':
                measure_data = entry.split('/')
                #return measure_data[-1].lower() == 'quit' or measure_data[-1].lower() == 'q'
                measure_tonality = twelve_tones[measure_data[0].upper()]
                octave = int(measure_data[1])
                measure_notes = measure_data[2]
                
                
                #note_generator
                note_objects = note_generator(measure_notes,measure_octave=octave)
                
                #measure_outputter
                formatted = measure_outputter(measure_num=m_num, measure=note_objects, tonality=measure_tonality)
                measures2tsv(m_voice,formatted)
                measure_counter = m_num
                measure_counter += 1
                note_entry(m_voice=m_voice,m_num=measure_counter)
            else:
            #return measure_data
                print('Thank you!')
                return
        note_entry()

#DATADIR = os.path.join(os.path.expanduser('~'),'abc_6950_hw','defuguer','score')
#print(os.path.exists(DATADIR))
#file_name = os.path.join(DATADIR,'soprano.tsv')

#create a DataFrame


#soprano_part = pd.read_csv(f1, sep='\t', names=['measure_num','numeric','absolute','tonality'])

#sp = soprano_part.set_index([x for x in range[0, len(soprano_part)]])

#create a function that retrieves data from tsv file
#0 - measure_num; 1 - numeric; 2 - absolute; 3 - tonality


def tsv2List(voice,measure_num,DATADIR=os.path.join(os.path.expanduser('~'),'abc_6950_HW','defuguer','score')):
    """Returns a measure object with data from .tsv file"""
    fname = os.path.join(DATADIR, voice+'.tsv')
    f0 = open(fname,'r')
    
    #select the appropriate line
    line = f0.readlines()[measure_num-1].strip('\n').split('\t')
    f0.close()
    #return the line in a tuple
    return tuple(line)
    
def measure_maker(line_in):
    """Takes output from tsv2List and returns a measure object"""
    
    measure_number = line_in[0]
    numeric_measure = line_in[1]
    absolute_measure = line_in[2]
    measure_tonality = 1
    return measure(numeric=numeric_measure,tonality=measure_tonality, measure_num=measure_number, absolute=absolute_measure)    
    
def tsv2Measure(voice, measure_num,DATADIR=os.path.join(os.path.expanduser('~'),'abc_6950_HW','defuguer','score')):
    """Reads a line from a tsv file and returns an object with the attributes from that line"""
    line=tsv2List(voice,measure_num,DATADIR)
    return measure_maker(line)