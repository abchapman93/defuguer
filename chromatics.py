#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 10:58:50 2016

@author: alec
"""

import formatting
import df_objects
tsv2Measure = formatting.tsv2Measure
import matplotlib.pyplot as plt

measure = df_objects.measure

Cs_major = [1, 3, 5, 6, 8, 10, 0]

def diatonic_tester(measure_in, tonality=[1, 3, 5, 6, 8, 10, 0]):
    #will probably want to import tonalities into this
    """takes 2 arguments: a phrase made up of a list with lists as measures and a tonality from dictionary'well_tempered'.
    Takes the notes from measures, which must be in numerical form, and tests what percentage of them are in the given 
        tonality."""
    numeric = measure_in.get_numeric()
    just_ints = [x for x in numeric if isinstance(x,int)]
    #for x in numeric:
    #    phrase_group.append(x)
    try:
        diatonic_rate = len([x for x in just_ints if x in tonality])/len(just_ints)
    except ZeroDivisionError:
        diatonic_rate=1
    return diatonic_rate
    
def graph_chromaticism(measures_num,ton=[1, 3, 5, 6, 8, 10, 0],all_voices=True,
                       soprano=False,tenor=False,bass=True):
    """Takes a range of measures as a tuple and graphs the chromaticism in that range.
    By default averages the chromaticism of all 3 voices."""
    all_chromatic = 0
    x_axis = []
    y_axis = []
    if all_voices==True:
        voice_names = ['soprano','tenor','bass']
        for number in [x for x in range(measures_num[0],measures_num[1]+1)]:
            x_axis.append(number)
            measure_chromatic = 0
            for voice in voice_names:
                voice_name = voice_names[voice_names.index(voice)]
                measure_object=tsv2Measure(voice_name,number)
                voice_chromatic = (1- diatonic_tester(measure_object))
                measure_chromatic += voice_chromatic
            measure_chromatic = measure_chromatic/3
            y_axis.append(measure_chromatic)
    climax = x_axis[y_axis.index(max(y_axis))]
    plt.plot(x_axis,y_axis)
              
    print('Most chromatic measure:', climax)
    return plt.show() 
    
