#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 15:26:35 2016

@author: alec
"""
import df_objects
import formatting

motives = {'subject':'''7,7,9,7,5,7,4,4,0,0,7,7,5,4,5,5,2,2,4,4,0,0,2,2,11,11,0,0'''
           }

def match_checker(theme,comp_measure):
    """Checks if a given measure matches the motive.
    match_rate > 0.5 is flagged as a match.
    Takes strings as arguments."""
    matches = 0
    for x in range(0,len(theme)):
        try:
            if theme[x]==comp_measure[x]:
                matches+=1
        except IndexError:
            pass
    match_rate = matches/len(theme)
    return match_rate
    
def match_format(measures,comp_type='exact'):
    """Takes a list of measure objects and positional argument
    Returns a string that is compatible with the appropriate measure method."""
    long_string_C = ''
    long_string_neut = ''
    if comp_type == 'exact':
        #transpose2C
        for x in measures:
            for note in x.transpose2C():
                long_string_C += str(note) + ','
            #neutralize
            for note in x.neutralize():
                long_string_neut += str(note) + ','
        return long_string_C, long_string_neut
    if comp_type == 'inversion':
        #create a new measure with inversion
        for x in measures:
            invert_string = ''
            for note in x.invert():
                invert_string += str(note)+','
                inv_measure = df_objects.measure(numeric=invert_string,tonality=x.get_tonality(),absolute=None)
                return inv_measure
                #transpose2C
                for note in inv_measure.transpose2C():
                    long_string_C += str(note) + ','
                #neutralize
                for note in inv_measure.neutralize():
                    long_string_neut += str(note) + ','
        return long_string_C, long_string_neut
                
        
def theme_matcher(motive,comparison,thresh=0.5):
    """Takes a motive as a string and a list of measure objects.
    if either match of neutralize or transpose2C >= .5
    Comparison is a list of measure objects."""
    #This should eventually just take one long string of modified measures
    #and check the motive to find instances
    
    #create strings from measures in comparison
    
    
    #identical
    #tranpose2C
    
    long_string = ''
    for measure in comparison:
        for note in measure.transpose2C():
            long_string += str(note) + ','
    long_string = long_string.strip('r,').strip('r').strip(' ')
    trans_match = match_checker(motive,long_string)
    if trans_match >= thresh:
        print("Probability of match: %s. Type of match: Identical"%str(trans_match))
        return
    else:
        pass
    
    #identical
    #neutralize
    long_string = ''
    for measure in comparison:
            for note in measure.neutralize():
                long_string += str(note) + ','
    long_string = long_string.strip('r,').strip('r').strip(' ')
    neut_match = match_checker(motive,long_string)
    if neut_match >= thresh:
        print("Probability of match: %s. Type of match: Identical"%str(neut_match)) 
        return
    else:
        pass
    
    #Now with the inversion
    
    