#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 13 14:28:16 2016

@author: alec
"""

"""This package assists in analyzing Bach's Fugue in C# Major from The Well-
Tempered Klavier Book 1.

See README for a more detailed description of the package.

See defuguer_demo.ipynb for a notebook designed to highlight the main functions
of the project. This notebook also includes a YouTube video demonstrating the 
project.

version 1

License: MIT

"""

__all__ = ["formatting","df_objects","graph_measures","musikgeist","chromatics"]