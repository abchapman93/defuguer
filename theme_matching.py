#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 18 15:02:10 2016

@author: alec
"""

import df_objects
import formatting

motives = {'subject1':'''7,7,9,7,5,7,4,4,0,0'''
           }

def match_checker(theme,comp_measure):
    """Checks if a given measure matches the motive.
    match_rate > 0.5 is flagged as a match.
    Takes strings as arguments."""
    matches = 0
    for x in range(0,len(theme)):
        try:
            if theme[x]==comp_measure[x]:
                matches+=1
        except IndexError:
            pass
    match_rate = matches/len(theme)
    return match_rate

def theme_matcher(motive,comparison,thresh=0.5,comparison_type='identical'):
    """Takes a motive as a string and a list of measure objects.
    if either match of neutralize or transpose2C >= .5
    Comparison is a list of measure objects."""
    #This should eventually just take one long string of modified measures
    #and check the motive to find instances
    
    #create strings from measures in comparison
    
    
    #identical
    #tranpose2C
    
    long_string = ''
    for note in comparison.transpose2C():
        long_string += str(note) + ','
    long_string = long_string.strip('r,').strip('r').strip(' ')
    #return long_string
    trans_match = match_checker(motive,long_string)
    if trans_match >= thresh:
        print("Probability of match: %s. Type of match: Identical"%str(trans_match))
        return
    else:
        pass
    
    #identical
    #neutralize
    long_string = ''
    for note in comparison.neutralize():
        long_string += str(note) + ','
    long_string = long_string.strip('r,').strip('r').strip(' ')
    neut_match = match_checker(motive,long_string)
    if neut_match >= thresh:
        print("Probability of match: %s. Type of match: Identical"%str(neut_match)) 
        return
    else:
        return "No match"