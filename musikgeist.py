#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 18 16:05:42 2016

@author: alec
"""
import os
import glob
import formatting as fm
import graph_measures as gm
import df_objects as do
import chromatics
import theme_matching as tm
from IPython.display import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from IPython.display import clear_output

resources = os.path.join(os.getcwd(),'resources')
os.path.exists(resources)
Note=do.Note
measure=do.measure
twelve_tones = fm.twelve_tones

def musikGeist(action=None):
    avatar=mpimg.imread(os.path.join(resources,'bach_shades.jpg'))
    
    
    if action == None:
        avatar=mpimg.imread(os.path.join(resources,'bach_shades.jpg'))
        
        action = input("""Sehr geehrte Daamen und Herren! I am your host for the evening, Johnny S. Bach
        \n 
        I'm a little under the weather today, but once I'm feeling better, look at all the exciting features I'll have to offer!
        \n 

        1 - Show me a measure
        2 - View the chromaticism of a section
        3 - View a visual of your beautiful fugue
        4 - How many kids did you have?)
        Q - Quit \n""")
        plt.imshow(avatar)
    if action == str(1):
        measure_number = int(input('Which measure would you like to see?'))
        print('soprano:',fm.tsv2Measure('soprano',measure_number).get_numeric())
        print('\n')
        print('tenor:',fm.tsv2Measure('tenor',measure_number).get_numeric())
        print('\n')
        print('bass:',fm.tsv2Measure('bass',measure_number).get_numeric())
        reset = input('Would you like to see another measure? Y/N')
        if reset.upper() == 'Y':
            musikGeist(action=str(0))
        else:
            clear_output()
            musikGeist(action=input("""How else can I help you? \n
            1 - Show me a measure
            2 - View the chromaticism of a section
            3 - View a visual of your beautiful fugue
            4 - How many kids did you have?)
            Q - Quit \n"""))
    if action == str(2): #Why doesn't this actually show a measure?
        measure_range = input("Please enter your the beginning and ending measure numbers for the section you'd like to see.")
        start = int(measure_range.split(',')[0])
        end = int(measure_range.split(',')[1])
        clear_output()
        chromatics.graph_chromaticism((start,end))
        reset = input('Would you like to see another section? Y/N')
        if reset.upper() == 'Y':
            musikGeist(action=str(0))
        else:
            clear_output()
            musikGeist(action=input("""How else can I help you? \n
            1 - Show me a measure
            2 - View the chromaticism of a section
            3 - View a visual of your beautiful fugue
            4 - How many kids did you have?)
            Q - Quit \n"""))
    if action == str(3):
        measure_range = input("Please enter your the beginning and ending measure numbers for the section you'd like to see.")
        start = int(measure_range.split(',')[0])
        end = int(measure_range.split(',')[1])
        clear_output()
        gm.graph_measures((start,end))
        reset = input('Would you like to see another section? Y/N')
        if reset.upper() == 'Y':
            musikGeist(action=str(3))
        else:
            clear_output()
            musikGeist(action=input("""How else can I help you? \n
            1 - Show me a measure
            2 - View the chromaticism of a section
            3 - View a visual of your beautiful fugue
            4 - How many kids did you have?)
            Q - Quit \n"""))
    if action == str(4):
        print("I had 20 kleine, süße Kindchen. And for the record, I composed more than 1,000 pieces. Everyone must have their hobbies!")
        musikGeist(action=input("""How else can I help you? \n
            1 - Show me a measure
            2 - View the chromaticism of a section
            3 - View a visual of your beautiful fugue
            4 - How many kids did you have?)
            Q - Quit \n"""))
    if action.upper() == 'Q':
        clear_output()
        avatar=mpimg.imread(os.path.join(resources,'bach_shades.jpg'))
        plt.imshow(avatar)
        print("Tschüß!")
        return